[[!meta title="Minutes from previous board meetings"]]
[[!meta copyright="Copyright © 2010-2022 Software in the Public Interest, Inc."]]
[[!meta license="Creative Commons Attribution-ShareAlike 3.0 Unported"]]

# Minutes from previous board meetings

These are the minutes of recent SPI board meetings:

* [Monday, 13th June, 2022](2022/2022-06-13)
* [Monday, 9th May, 2022](2022/2022-05-09)
* [Monday, 11th April, 2022](2022/2022-04-11)
* [Monday, 28th March, 2022](2022/2022-03-28)
* [Monday, 14th March, 2022](2022/2022-03-14)
* [Monday, 14th February, 2022](2022/2022-02-14)
* There was no meeting in January 2022

Minutes by year:

[[!inline pages="meetings/minutes/* and ! meetings/minutes/*/*" sort="-title" template=unordered-list-title archive=yes]]
